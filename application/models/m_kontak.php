<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class m_kontak extends CI_Model {

	public function get() {
		return $this->db->get('tb_m_kontak')->result();
	}

	public function insert($data, $table) {
		$this->db->insert($table, $data);
	}

	public function edit($id, $data, $table) {
		$this->db->where($id);
		$this->db->update($table, $data);
	}

	public function delete($id, $table) {
		$this->db->where_in('id', $id);
		$this->db->delete($table);
	}

	public function getInfoSekolah() {
		return $this->db->query('SELECT * FROM tb_m_kontak WHERE id = "1"')->result();
	}

}    
