<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_guru extends CI_Controller {
	public function index()
	{
		$title['title'] = 'Akun Guru';
		$data = [
			'guru'	=> $this->m_guru->getAllGuru()
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/guru/list_guru',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function profilGuru($ids)
	{
		$id 		= ['id' => $ids];
		$nama 		= $this->db->get_where('tb_m_guru',$id)->row_array();
		$title['title'] = 'Profil Guru | '.$nama['nama_guru'];
		$data = [
			'guru'	=> $this->m_guru->getGuruProfile($ids)->result()
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/guru/profil_guru',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function insertGuru()
	{
		$this->form_validation->set_rules('nama_lengkap','Nama Lengkap', 'required',
    		['required' => 'Nama Lengkap harus diisi!']);
		$this->form_validation->set_rules('mapel','Mata Pelajaran', 'required',
    		['required' => 'Mohon pilih salah satu!']);
		$this->form_validation->set_rules('tgl_masuk','Tanggal Masuk', 'required',
    		['required' => 'Tanggal Masuk harus diisi!']);
		$this->form_validation->set_rules('email','Email', 'required|valid_email|is_unique[auth.username]',
    		['required'    => 'Email harus diisi!',
    		 'valid_email' => 'Masukan Email dengan benar!',
    		 'is_unique'   => 'Email sudah terdaftar!']);
		$this->form_validation->set_rules('password','Password', 'required',
    		['required' => 'Password harus diisi!']);

		if ($this->form_validation->run()== false) {
			$title['title'] = 'Akun Guru';
			$data = [
				'guru'	=> $this->crud->get('tb_m_guru')
				];

			$this->load->view('templates/server_partial/script_css',$title);
			$this->load->view('templates/server_partial/header');
			$this->load->view('templates/server_partial/sidebar');
			$this->load->view('server/guru/list_guru',$data);
			$this->load->view('templates/server_partial/footer');
			$this->load->view('templates/server_partial/script_js');
		}else{
			$nama_lengkap	= $this->input->post('nama_lengkap');
			$mapel			= $this->input->post('mapel');
			$tgl_masuk		= $this->input->post('tgl_masuk');
			$email			= $this->input->post('email');
			$password		= $this->input->post('password');

				$config['upload_path']		= './assets/images/guru_images/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['encrypt_name']		=  TRUE;
				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('foto')){
					$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
    				Redirect('Admin_guru');
				}else{
					$foto 	  = $this->upload->data('file_name');
					$dataGuru = [
						'nama_guru'	=> $nama_lengkap,
						'mapel'		=> $mapel,
						'tgl_masuk' => $tgl_masuk,
						'foto'		=> $foto,
						'created_by' => 'ADMIN'
					];
					$this->crud->insert($dataGuru,'tb_m_guru');
					$insert_id = $this->db->insert_id();
					$dataAuth = [
						'username'	=> $email,
						'password'  => password_hash($password, PASSWORD_DEFAULT),
						'role'  	=> 'guru',
						'guru_id'	=> $insert_id,
						'created_by' => 'ADMIN'
					];
					$this->crud->insert($dataAuth,'auth');
					$this->session->set_flashdata('success' , 'Pendaftaran Berhasil!');
					Redirect('Admin_guru');
		}
		}
	}

	public function postEdit($ids) {
		$id 			= ['id' => $ids];
		$auth 			= $this->db->get_where('auth',$id)->row_array();
		$nama_guru		= $this->input->post('nama_lengkap');
		$mapel 			= $this->input->post('mapel');
		$tgl_masuk 		= $this->input->post('tgl_masuk');
		$gmail 			= $this->input->post('gmail');
		$facebook 		= $this->input->post('facebook');
		$instagram 		= $this->input->post('instagram');
		// $pass_lama 		= $this->input->post('pass_lama');
		// $pass_baru 		= $this->input->post('pass_baru');
		$foto_lama 		= $this->input->post('foto_lama');
		$foto 			= $this->input->post('foto');

		if ($foto !== '') {  
			// $this->crud->deletePhoto($foto_lama,'guru_images');
			$config['upload_path']		= './assets/images/guru_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $nama_guru.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}
		// if (password_verify($pass_lama, $auth['password'])) 
			$dataGuru = [
				'nama_guru'	=> $nama_guru,
				'mapel'		=> $mapel,
				'tgl_masuk' => $tgl_masuk,
				'gmail'		=> $gmail,
				'foto'		=> $foto,
				'facebook'	=> $facebook,
				'instagram' => $instagram
			];
			// $dataAuth = [
			// 	'username'	=> $email,
			// 	'password'  => $password
			// ];
			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'guru_images');
			}
			$this->crud->edit($id,$dataGuru,'tb_m_guru');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			if ($this->session->userdata('role') == 'guru') {
				redirect('Admin_guru/profileGuruSingle');
			}
				redirect(base_url('Admin_guru/profilGuru/').$ids);
		
	}

	public function deleteGuru($id)
	{
		$this->crud->delete($id,'tb_m_guru');
		$this->session->set_flashdata('success','Sukses hapus data!');
		Redirect('Admin_guru');
	}

	public function profileGuruSingle()
	{
		$title['title'] = 'Profil Guru';
		$data = [
			'guru'	=> $this->crud->get('tb_m_guru')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/guru/profil_guru',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function gantiEmail($ids)
	{
			$this->form_validation->set_rules('email_baru', 'Email', 'required|valid_email|is_unique[auth.username]', 
				['required' => 'email harus diisi',
				'valid_email' => 'masukkan email yang benar',
				'is_unique' => 'email sudah terdaftar']);

			if ($this->form_validation->run() == false) {
				$id 		= ['id' => $ids];
				$nama 		= $this->db->get_where('tb_m_guru',$id)->row_array();
				$title['title'] = 'Profil Guru | '.$nama['nama_guru'];
				$data = [
					'guru'	=> $this->m_guru->getGuruProfile($ids)
					];

				$this->load->view('templates/server_partial/script_css',$title);
				$this->load->view('templates/server_partial/header');
				$this->load->view('templates/server_partial/sidebar');
				$this->load->view('server/guru/profil_guru',$data);
				$this->load->view('templates/server_partial/footer');
				$this->load->view('templates/server_partial/script_js');
			}else{
				$id 		= ['guru_id' => $ids];
				$emailBaru = $this->input->post('email_baru');
				$data = [
					'username' => $emailBaru
				];
				$this->crud->edit($id, $data, 'auth');
				$this->session->set_flashdata('success', 'Email berhasil diganti!');
				Redirect('Admin_guru/profilGuru/'.$ids);
			}
	}

	public function gantiPassword($ids)
	{
		$this->form_validation->set_rules('password_lama', 'Retype Password', 'required',
											['required' => 'password lama harus diisi!']);
		$this->form_validation->set_rules('password_baru', 'Password', 'required', 
											['required' => 'password baru harus diisi']);

		if ($this->form_validation->run() == false) {
				$id 		= ['id' => $ids];
				$nama 		= $this->db->get_where('tb_m_guru',$id)->row_array();
				$title['title'] = 'Profil Guru | '.$nama['nama_guru'];
				$data = [
					'guru'	=> $this->m_guru->getGuruProfile($ids)->result()
					];

				$this->load->view('templates/server_partial/script_css',$title);
				$this->load->view('templates/server_partial/header');
				$this->load->view('templates/server_partial/sidebar');
				$this->load->view('server/guru/profil_guru',$data);
				$this->load->view('templates/server_partial/footer');
				$this->load->view('templates/server_partial/script_js');
			}else{
				$id = ['guru_id' => $ids];
				$auth = $this->db->get_where('auth', $id)->row_array();
				$passwordLama = $this->input->post('password_lama');
				$passwordBaru = $this->input->post('password_baru');
				if (password_verify($passwordLama, $auth['password'])) {
					$data = [
						'password' => password_hash($passwordBaru, PASSWORD_DEFAULT) 
					];
					var_dump($data);
					$this->crud->edit($id, $data, 'auth');
					$this->session->set_flashdata('success', 'Password berhasil diganti!');
					Redirect('Admin_guru/profilGuru/'.$ids);
				}else{
					$this->session->set_flashdata('fail', 'Password lama tidak sesuai!');
					Redirect('Admin_guru/profilGuru/'.$ids);
				}
			}
	}

}
