<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller {

	public function index() {
		$data['title'] = 'Beranda';
		$data['galeri_kegiatan'] 	= $this->m_galeri->getFotoLimit();
		$data['prestasi'] 			= $this->m_prestasi->getPrestasiLimit();
		$data['guru'] 				= $this->m_guru->getGuruLimit();
		$data['beranda'] 			= $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('index', $data);
		$this->load->view('templates/footer');
	}

	public function strukturOrganisasi() {
		$data['title'] = 'Struktur Organisasi';
		$data['struktur_organisasi'] = $this->m_struktur_organisasi->get();
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('profil/struktur_organisasi', $data);
		$this->load->view('templates/footer');
	}

	public function jurusanSekolah() {
		$data['title'] = 'Jurusan Sekolah';
		$data['jurusan'] = $this->m_jurusan->get();
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('profil/jurusan_sekolah', $data);
		$this->load->view('templates/footer');
	}

	public function ppdb() {
		$data['title'] = 'Pendaftaran';
		$data['ppdb'] = $this->crud->get('tb_m_kontak');
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('ppdb', $data);
		$this->load->view('templates/footer');
	}

	public function insertPpdb()
	{
		$this->form_validation->set_rules('nama_siswa','Nama Siswa', 'required',
    		['required' => 'Nama Siswa harus diisi!']);

		if ($this->form_validation->run() == false) {
			
			$this->session->set_flashdata('gagal', '
			<div class="alert alert-warning alert-dismissible fade show" role="alert">
			Pendaftaran Gagal, Silahkan coba lagi!
			<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
			</div>
			');

			$this->load->view('templates/navbar');
			$this->load->view('ppdb');
			$this->load->view('templates/footer');
		}else{
				$data = array();
				$nama_siswa		= $this->input->post('nama_siswa');
				
				$config['upload_path']		= './assets/images/ppdb_images/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['encrypt_name']		=  TRUE;
				$this->load->library('upload', $config);

				if($this->upload->do_upload('ijazah')) {
					$fileData = $this->upload->data();
    				$data['ijazah'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('sku')) {
					$fileData = $this->upload->data();
    				$data['sku'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('akk')) {
					$fileData = $this->upload->data();
    				$data['akk'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('kk')) {
					$fileData = $this->upload->data();
    				$data['kk'] = $fileData['file_name'];
				}
				if($this->upload->do_upload('pernyataan')) {
					$fileData = $this->upload->data();
    				$data['pernyataan'] = $fileData['file_name'];
				}

					$dataSiswa = [
						'nama_siswa'	=> $nama_siswa,
						'ijazah'		=> $data['ijazah'],
						'sku'			=> $data['sku'],
						'akk'			=> $data['akk'],
						'kk'			=> $data['kk'],
						'pernyataan'	=> $data['pernyataan'],
						'created_by'	=> $nama_siswa
					];
					$this->crud->insert($dataSiswa,'tb_m_ppdb');
					$this->session->set_flashdata('success' , 'Berhasil mendaftar!');
					Redirect('Page/ppdb');
			
		}
	}

	public function guru() {
		$data['title'] = 'Guru';
		$data['guru'] = $this->crud->get('tb_m_guru');
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('guru', $data);
		$this->load->view('templates/footer');
	}

	public function foto() {
		$data['title'] = 'Foto';
		$data['foto'] = $this->m_galeri->getFoto();
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('galeri/foto', $data);
		$this->load->view('templates/footer');
	}

	public function video() {
		$data['title'] = 'Video';
		$data['video'] = $this->m_galeri->getVideo();
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('galeri/video', $data);
		$this->load->view('templates/footer');
	}

	public function prestasi() {
		$data['title'] = 'Prestasi';
		$data['prestasi'] = $this->m_prestasi->get();
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
		$this->load->view('prestasi', $data);
		$this->load->view('templates/footer');
	}

	public function kirimEmail($email,$subject,$isi)
	{
		$config = [
            'mailtype'  => 'html',
            'charset'   => 'utf-8',
            'protocol'  => 'smtp',
            'smtp_host' => 'smtp.gmail.com',
            'smtp_user' => 'arkamayanet@gmail.com',  // Email gmail
            'smtp_pass'   => 'guntursari17',  // Password gmail
            'smtp_crypto' => 'ssl',
            'smtp_port'   => 465,
            'crlf'    => "\r\n",
            'newline' => "\r\n"
        	];

        $this->email->initialize($config);
		$this->email->from('arkamayanet@gmail.com', 'SISFO');
		$this->email->to($email);

		$this->email->subject($subject);
		$this->email->message($isi);

		if (!$this->email->send())
			{
				$this->session->set_flashdata('success' , '
					<div class="alert alert-success alert-dismissible fade show" role="alert">
					Pendaftaran <strong> GAGAL!</strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					');
			    Redirect('Page');
			}else{
				$this->session->set_flashdata('success' , '
					<div class="alert alert-success alert-dismissible fade show" role="alert">
					Pendaftaran <strong> Berhasil!</strong>
					<button type="button" class="close" data-dismiss="alert" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					');
				Redirect('Page/contactUs');
			}
	}

	public function contactUs() {
		$this->form_validation->set_rules('nama', 'Nama', 'required', 
																			['required' => 'nama harus diisi!']);
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email', 
																			['required' => 'email harus diisi!',
																			'valid_email' => 'masukkan email yang benar!']);
		$this->form_validation->set_rules('pesan', 'pesan', 'required', 
																		['required' => 'pesan harus diisi!']);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Hubungi';
			$data['contact_us'] = $this->crud->get('tb_m_kontak');
			$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $data);
			$this->load->view('contact_us', $data);
			$this->load->view('templates/footer');
		} else {
			$nama = $this->input->post('nama');
			$email = $this->input->post('email');
			$pesan = $this->input->post('pesan');

			$namaDepan = explode(' ', $nama);
			$data = [
				'nama' => $nama,
				'email' => $email,
				'subject' => 'Feedback Pengguna',
				'deskripsi' => $pesan,
				'status' => 'belum_dibaca',
				'tipe_pesan' => 'penerima',
				'favorit' => 'false',
				'sampah' => 'false',
				'created_by' => $namaDepan[0]
			];

			$this->m_kontak->insert($data, 'tb_m_pesan');
			$this->session->set_flashdata('success', 'Berhasil mengirim pesan!');
			Redirect('Page/contactUs');
		}
	}

	public function login() {
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email', 
																			['required' => 'email harus diisi!',
																			'valid_email' => 'masukkan email yang benar!']);
		$this->form_validation->set_rules('password', 'Password', 'required', 
																		['required' => 'password harus diisi!']);
		
		$this->form_validation->set_rules('captcha', 'Captcha', 'required', 
																		['required' => 'Captcha harus diisi!']);

		if ($this->form_validation->run() == false) {
			$data['title'] = 'Login';
			$data['beranda'] = $this->crud->get('tb_m_beranda');

			$path = "./assets/captcha/";
		
				if (!file_exists($path)) {
					$createFolder = mkdir($path,0772);
					if ($createFolder) {
						return;
					}
				}
				$word = array_merge(range('0', '9',), range('A', 'Z'));
				$random = shuffle($word);
				$str = substr(implode($word), 0, 5);

				$dataSes = ["captcha_str" => $str];
				$this->session->set_userdata($dataSes);

				$vals = [
							"word" => $str,
							"img_path" => $path,
							"img_url" => base_url("assets/captcha/"),
							"img_width" => "150", 
							"img_height" => 40,
							"expiration" => 7200
				];

				$capc = create_captcha($vals);
				$data["captcha_image"] = $capc["image"];

			$this->load->view('templates/navbar', $data);
			$this->load->view('login', $data);
			$this->load->view('templates/footer');
		} else {
			$this->postlogin();
		}
	}

	private function postlogin() {
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$captcha = $this->input->post('captcha');

		if ($captcha != $this->session->userdata("captcha_str")) {
			$this->session->set_flashdata('fail', "Capthca salah!");
			Redirect('Page/login');
		}else{
		

		$auth = $this->db->get_where('auth', ['username' => $email])->row_array();
		if ($auth) {
			if (password_verify($password, $auth['password'])) {
				if($auth['role'] == 'admin') {
					$data = [
						'id' 		=> $auth['id'],
						'email' 	=> $auth['username'],
						'role' 		=> $auth['role']
					];
					$this->session->set_userdata($data);
					$this->session->set_flashdata('success', 'Berhasil login!');
					Redirect('Admin');
				}elseif ($auth['role'] == 'guru') {
					$data = [
						'id' 		=> $auth['guru_id'],
						'email' 	=> $auth['username'],
						'role' 		=> $auth['role']
					];
					$this->session->set_userdata($data);
					$this->session->set_flashdata('success', 'Berhasil login!');
					Redirect('Admin_guru/profileGuruSingle');
				}
			}else{
				$this->session->set_flashdata('fail', 'Password salah mohon coba lagi!');
				Redirect('Page/login');
			}
		}else{
			$this->session->set_flashdata('fail', 'Email salah mohon coba lagi!');
			Redirect('Page/login');
		}	
	}
	}

		public function logout() {
			$data = ['id', 'username', 'img', 'status', 'email', 'role'];
			$this->session->unset_userdata($data);
			$this->session->set_flashdata('success', 'Berhasil logout! ');	
			Redirect('Page/login');
		}

}
