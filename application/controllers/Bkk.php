<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bkk extends CI_Controller {
	public function lowonganKerja()
	{
		$title['title'] = 'Lowongan Kerja';
		$data['lowongan_kerja'] = $this->m_bkk->getLowongan();
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $title);
		$this->load->view('bkk/lowongan_kerja', $data);
		$this->load->view('templates/footer');

    }
    
	public function serapanKerja()
	{
		$title['title'] = 'Serapan Kerja';
		$data['serapan_kerja'] = $this->m_bkk->getSerapan();
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $title);
		$this->load->view('bkk/serapan_kerja', $data);
		$this->load->view('templates/footer');

	}

	public function detailLowongan($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_bkk',$id)->row_array();
		$title['title'] = 'Detail Lowongan | '.$nama['judul'];
		$data = [
			'lowongan'	=> $this->crud->getById('tb_m_bkk',$id)
			];
		$data['beranda'] = $this->crud->get('tb_m_beranda');

		$this->load->view('templates/navbar', $title);
		$this->load->view('bkk/detail_lowongan', $data);
		$this->load->view('templates/footer');
	}

	public function detailSerapan($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_bkk',$id)->row_array();
		$title['title'] = 'Detail Serapan Kerja | '.$nama['judul'];
		$data = [
			'serapan'	=> $this->crud->getById('tb_m_bkk',$id)
			];
		$data['beranda'] = $this->crud->get('tb_m_beranda');	

		$this->load->view('templates/navbar', $title);
		$this->load->view('bkk/detail_serapan', $data);
		$this->load->view('templates/footer');
	}
}