<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_prestasi extends CI_Controller {
	public function listPrestasi()
	{
		$title['title'] = 'List Prestasi';
		$data = [
			'prestasi'	=> $this->crud->get('tb_m_prestasi')
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/prestasi/list_prestasi',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');

	}

	public function detailPrestasi($id)
	{
		$id = ['id' => $id];
		$nama 		= $this->db->get_where('tb_m_prestasi',$id)->row_array();
		$title['title'] = 'Detail Prestasi | '.$nama['nama_prestasi'];
		$data = [
			'prestasi'	=> $this->crud->getById('tb_m_prestasi',$id)
			];

		$this->load->view('templates/server_partial/script_css',$title);
		$this->load->view('templates/server_partial/header');
		$this->load->view('templates/server_partial/sidebar');
		$this->load->view('server/front_end/prestasi/detail_prestasi',$data);
		$this->load->view('templates/server_partial/footer');
		$this->load->view('templates/server_partial/script_js');
	}

	public function insertPrestasi()
	{
		$this->form_validation->set_rules('nama_prestasi','Nama Prestasi', 'required',
    		['required' => 'Nama Prestasi harus diisi!']);
		$this->form_validation->set_rules('deskripsi','Deskripsi', 'required',
    		['required' => 'Deskripsi harus diisi!']);
		$this->form_validation->set_rules('tgl_prestasi','Tanggal Prestasi', 'required',
    		['required' => 'Tanggal Prestasi harus diisi!']);

		if ($this->form_validation->run()== false) {
			$title['title'] = 'List Prestasi';
			$data = [
				'prestasi'	=> $this->crud->get('tb_m_prestasi')
				];

			$this->load->view('templates/server_partial/script_css',$title);
			$this->load->view('templates/server_partial/header');
			$this->load->view('templates/server_partial/sidebar');
			$this->load->view('server/front_end/prestasi/list_prestasi',$data);
			$this->load->view('templates/server_partial/footer');
			$this->load->view('templates/server_partial/script_js');
		}else{
			$nama_prestasi	= $this->input->post('nama_prestasi');
			$deskripsi		= $this->input->post('deskripsi');
			$tgl_prestasi	= $this->input->post('tgl_prestasi');

				$config['upload_path']		= './assets/images/prestasi_images/';
				$config['allowed_types']	= 'jpg|png|jpeg';
				$config['file_name']		= $nama_prestasi.'-'.$tgl_prestasi;
				$this->load->library('upload', $config);

				if(!$this->upload->do_upload('foto')){
					$this->session->set_flashdata('fail', 'Kesalahan mengunggah gambar!');
    				Redirect('Admin_prestasi/listPrestasi');
				}else{
					$foto 	  = $this->upload->data('file_name');
					$data = [
						'foto'				=> $foto,
						'nama_prestasi'		=> $nama_prestasi,
						'deskripsi'			=> $deskripsi,
						'tanggal_prestasi'  => $tgl_prestasi
					];
					$this->crud->insert($data,'tb_m_prestasi');
					$this->session->set_flashdata('success','Pendaftaran Berhasil!');
					Redirect('Admin_prestasi/listPrestasi');
			}
		}

	}

	public function postEditPrestasi($ids) {
		$id 				= ['id' => $ids];
		$nama_prestasi		= $this->input->post('nama_prestasi');
		$deskripsi 			= $this->input->post('deskripsi');
		$tanggal_prestasi 	= $this->input->post('tgl_prestasi');
		$foto  				= $this->input->post('foto');
		$foto_lama			= $this->input->post('foto_lama');

		if ($foto !== '') {  
			$config['upload_path']		= './assets/images/prestasi_images/';
			$config['allowed_types']	= 'jpg|png|jpeg';
			$config['file_name']		= $nama_prestasi.'-'.date('y-m-d');
			$this->load->library('upload', $config);

			if(!$this->upload->do_upload('foto')){
				$foto = $foto_lama;
				$hapus = 'false';
    		}else{
    			$upload_data	= $this->upload->data();
    			$foto 	  		= $upload_data['file_name'];
    			$hapus = 'true';
   			}

		}
			
			$data = [
				'foto'				=> $foto,
				'nama_prestasi'		=> $nama_prestasi,
				'deskripsi'			=> $deskripsi,
				'tanggal_prestasi' 	=> $tanggal_prestasi
			];

			if ($hapus == 'true') {
				$this->crud->deletePhoto($foto_lama,'jurusan_images');
			}
			$this->crud->edit($id,$data,'tb_m_prestasi');
			$this->session->set_flashdata('success', 'Data berhasil diperbarui!');
			redirect(base_url('Admin_prestasi/detailPrestasi/').$ids);
		
	}
}