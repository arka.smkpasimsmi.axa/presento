 
 <!-- ======= Guru Section ======= -->
 <section id="team" class="team">
    <div class="container mt-5" data-aos="fade-up">
		<?php foreach($beranda as $data) : ?>
		<div class="section-title">
			<h2>Profil Guru</h2>
			<p>Berikut merupakan profil guru dari <?= $data->nama_sekolah; ?>.</p>
		</div>
		<?php endforeach; ?>
		
		<div class="row">
	<?php foreach ($guru as $data) : ?>	
	
        <div class="col-lg-4 col-md-4 d-flex align-items-stretch">
          <div class="member" style="border:1px solid #cbcbcb;" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
				<a href="<?= base_url('assets/images/guru_images/'.$data->foto); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->foto; ?>">
              		<img style="width:100%;max-height: 250px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/guru_images/'.$data->foto); ?>" class="img-fluid" alt="">
				</a>
			<div class="social">
				<a href=""><i class="icofont-twitter"></i></a>
				<a href=""><i class="icofont-facebook"></i></a>
				<a href=""><i class="icofont-instagram"></i></a>
				<a href=""><i class="icofont-linkedin"></i></a>
			</div>	
			</div>
            <div class="member-info">
              <h4><?= $data->nama_guru; ?></h4>
              <p><?= $data->mapel; ?></p>
            </div>
          </div>
		</div>
		
	<?php endforeach; ?>

      </div>

    </div>
  </section><!-- End Guru Section -->