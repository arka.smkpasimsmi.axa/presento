 
 <!-- ======= Struktur Section ======= -->
 <section id="team" class="team">
    <div class="container mt-5" data-aos="fade-up">
		<?php foreach($beranda as $data) : ?>
		<div class="section-title">
			<h2>Struktur Organisasi</h2>
			<p>Berikut merupakan struktur organisasi dari <?= $data->nama_sekolah; ?>.</p>
		</div>
		<?php endforeach; ?>
		
		<div class="row">
	<?php foreach ($struktur_organisasi as $data) : ?>	
	
        <div class="col-lg-12 col-md-12 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
				<a href="<?= base_url('assets/images/struktur_images/'.$data->foto); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->foto; ?>">
              		<img style="width:100%;cursor:pointer;" src="<?= base_url('assets/images/struktur_images/'.$data->foto); ?>" class="img-fluid" alt="">
				</a>
			</div>
            <div class="member-info">
              <p><?= $data->deskripsi; ?></p>
            </div>
          </div>
		</div>
		
	<?php endforeach; ?>

      </div>

    </div>
  </section><!-- End Struktur Section -->