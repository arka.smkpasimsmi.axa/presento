 <!-- ======= jurusan Section ======= -->
 <section id="team" class="team">
    <div class="container mt-5" data-aos="fade-up">
		<?php foreach($beranda as $data) : ?>
			<div class="section-title">
				<h2>Jurusan Sekolah</h2>
				<p>Berikut merupakan jurusan sekolah dari <?= $data->nama_sekolah; ?>.</p>
			</div>
		<?php endforeach; ?>
						<?php $no=1; ?>
							<?php foreach ($jurusan as $data): ?>
								<?php $no++; ?>
									<?php if($no % 2 == 0) : ?>
									<!-- Info Jurusan Kerja Kiri -->
										<div class="container mt-4 pt-4">
										<div class="row">
											<div class="col-md-6">
												<div class="mt-4">
														<h4 class="course_title"><?= $data->nama_jurusan ?></h4>
														<div class="course_text">
															<p><?= $data->deskripsi_jurusan; ?></p>
														</div>
												</div>
											</div>
									<!-- Jurusan Kiri -->
											<div class="col-md-6">
											<a href="<?= base_url('assets/images/jurusan_images/'.$data->foto); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->foto; ?>">
													<img style="width:100%;max-height: 340px;object-fit: cover;object-position: top;cursor:pointer;border-radius:5px;" src="<?= base_url('assets/images/jurusan_images/'.$data->foto) ?>" alt="">
												</a>
											</div>
										</div>
										</div>
									<?php else : ?>
									<!-- Jurusan Kanan -->
									<div class="container mt-5 mt-4 pt-4">
										<div class="row">
											<div class="col-md-6">
											<a href="<?= base_url('assets/images/jurusan_images/'.$data->foto); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->foto; ?>">
													<img style="width:100%;max-height: 340px;object-fit: cover;object-position: top;cursor:pointer;border-radius:5px;" src="<?= base_url('assets/images/jurusan_images/'.$data->foto) ?>" alt="">
												</a>
											</div>
									<!-- Info Jurusan Kanan -->
											<div class="col-md-6">
												<div class="mt-4">
													<h4 class="course_title"><?= $data->nama_jurusan ?></h4>
													<div class="course_text">
														<p><?= $data->deskripsi_jurusan; ?></p>
													</div>
												</div>
											</div>
										</div>
										</div>	
									<?php endif; ?>
							<?php endforeach; ?>
	</div>						
</section>
<!-- ======= End jurusan Section ======= -->
