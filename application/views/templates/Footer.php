<?php $kontak = $this->crud->get('tb_m_kontak'); ?>
<?php foreach ($kontak as $data) : ?>  
  <!-- ======= Footer ======= -->
  <footer id="footer">

    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-6 col-md-6 footer-contact">
            <h3><?= $data->nama_sekolah ?></h3>
            <p>
              <?= $data->alamat ?><br>
              <?= $data->kota ?><br>
              
            </p>

          </div>

          <div class="col-lg-6 col-md-6 footer-links">
            <h4>Kontak</h4>
            <ul>
              <strong>Telepon:</strong> <?= $data->telepon ?><br>
              <strong>Email:</strong> <?= $data->email_sekolah ?><br>
            </ul>
          </div>
          


        </div>
      </div>
    </div>

    <div class="container d-md-flex py-4">

      <div class="mr-md-auto text-center text-md-left">
        <div class="copyright">
          &copy; Copyright <strong><span>RPL Pasim 2020</strong>. All Rights Reserved
        </div>
        <div class="credits">
          <!-- All the links in the footer should remain intact. -->
          <!-- You can delete the links only if you purchased the pro version. -->
          <!-- Licensing information: https://bootstrapmade.com/license/ -->
          <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/presento-bootstrap-corporate-template/ -->
        </div>
      </div>
      <div class="social-links text-center text-md-right pt-3 pt-md-0">
        <a href="#" class="twitter"><i class="bx bxl-twitter"></i></a>
        <a href="#" class="facebook"><i class="bx bxl-facebook"></i></a>
        <a href="#" class="instagram"><i class="bx bxl-instagram"></i></a>
        <a href="#" class="google-plus"><i class="bx bxl-google"></i></a>
        <a href="#" class="linkedin"><i class="bx bxl-youtube"></i></a>
      </div>
    </div>
  </footer><!-- End Footer -->

<?php endforeach; ?>

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="<?= base_url('assets/js/jquery-3.2.1.min.js') ?>"></script>
  <script src="<?= base_url('assets/vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/jquery.easing/jquery.easing.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/php-email-form/validate.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/owl.carousel/owl.carousel.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/waypoints/jquery.waypoints.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/counterup/counterup.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/isotope-layout/isotope.pkgd.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/venobox/venobox.min.js'); ?>"></script>
  <script src="<?= base_url('assets/vendor/aos/aos.js'); ?>"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url('assets/js/main.js'); ?>"></script>

  <script src="<?= base_url('assets/server_assets/js/custom.js') ?>"></script>
  <script src="<?= base_url('assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.js') ?>"></script>
  <script src="<?= base_url('assets/server_assets/js/alerts.js') ?>"></script>

  <script>
        $(document).ready(function(){
            <?php if ($this->session->flashdata('success')) : ?>
                showSuccessToast("<?= $this->session->flashdata('success'); ?>"); //function ada di assets/server_assets/js/alerts.js
            <?php endif; ?>

            <?php if ($this->session->flashdata('fail')) : ?>
                showDangerToast("<?= $this->session->flashdata('fail'); ?>");
            <?php endif; ?>

            <?php if ($this->session->flashdata('info')) : ?>
                showInfoToast("<?= $this->session->flashdata('info'); ?>");
            <?php endif; ?>

             <?php if ($this->session->flashdata('warning')) : ?>
                showWarningToast("<?= $this->session->flashdata('warning'); ?>");
            <?php endif; ?>

          });
  </script>

</body>

</html>