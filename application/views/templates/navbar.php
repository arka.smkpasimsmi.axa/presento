<!DOCTYPE html>
<html lang="en">

<?php $beranda = $this->crud->get('tb_m_beranda') ?>
<?php $kontak = $this->crud->get('tb_m_kontak') ?>

		
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
	<?php foreach ($beranda as $data) : ?>
		<title><?= $data->nama_sekolah.' - '.$title ?></title>
		<!-- Favicons -->
		<link rel="icon" href="<?= base_url('assets/images/beranda_images/'.$data->ikon) ?>" type="image/x-icon" />
	<?php endforeach; ?>

  <meta content="" name="description">
  <meta content="" name="keywords">


  <!-- Google Fonts -->
  <link
    href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
    rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/icofont/icofont.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/boxicons/css/boxicons.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/owl.carousel/assets/owl.carousel.min.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/remixicon/remixicon.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/venobox/venobox.css'); ?>" rel="stylesheet">
  <link href="<?= base_url('assets/vendor/aos/aos.css'); ?>" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url('assets/css/style.css'); ?>" rel="stylesheet">

  <link rel="stylesheet" href="<?= base_url() ?>assets/server_assets/plugins/jquery-toast-plugin/dist/jquery.toast.min.css">
  

  <!-- =======================================================
  * Template Name: Presento - v1.0.0
  * Template URL: https://bootstrapmade.com/presento-bootstrap-portfolio-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top">
    <div class="container-fluid">
      <div class="row justify-content-center">
        <div class="col-xl-10 d-flex align-items-center">
		 <img style="width:35px;" src="<?= base_url('assets/images/beranda_images/'.$data->ikon); ?>" alt="">
          <h1 class="logo mr-auto ml-2"><a href="<?= base_url(''); ?>"><?= $data->teks_logo; ?></a></h1>
          <!-- Uncomment below if you prefer to use an image logo -->
          <!-- <a href="index.html" class="logo mr-auto"><img src="assets/img/logo.png" alt=""></a>-->

          <nav class="nav-menu d-none d-lg-block">
            <ul>
              <li class="<?php if($this->uri->segment(1) == "") {echo 'active';} ?>"><a href="<?= base_url(''); ?>">Beranda</a></li>
              <li class="drop-down <?php if($this->uri->segment(2) == "profil") {echo 'active';} ?>"><a href="">Profil</a>
                <ul>
                  <li class="<?php if($this->uri->segment(2) == "strukturOrganisasi") {echo 'active';} ?>"><a href="<?= base_url('Page/strukturOrganisasi'); ?>">Struktur Organisasi</a></li>
                  <li class="<?php if($this->uri->segment(2) == "jurusanSekolah") {echo 'active';} ?>"><a href="<?= base_url('Page/jurusanSekolah'); ?>">Jurusan Sekolah</a></li>
                </ul>
              </li>
              <li class="drop-down <?php if($this->uri->segment(2) == "galeri") {echo 'active';} ?>"><a href="">Galeri</a>
                <ul>
                  <li class="<?php if($this->uri->segment(2) == "foto") {echo 'active';} ?>"><a href="<?= base_url('Page/foto'); ?>">Foto</a></li>
                  <li class="<?php if($this->uri->segment(2) == "video") {echo 'active';} ?>"><a href="<?= base_url('Page/video'); ?>">Video</a></li>
                </ul>
              </li>
              <li class="<?php if($this->uri->segment(2) == "guru") {echo 'active';} ?>"><a href="<?= base_url('Page/guru'); ?>">Guru</a></li>
              <li class="<?php if($this->uri->segment(2) == "prestasi") {echo 'active';} ?>"><a href="<?= base_url('Page/prestasi'); ?>">Prestasi</a></li>
              <li class="<?php if($this->uri->segment(2) == "ppdb") {echo 'active';} ?>"><a href="<?= base_url('Page/ppdb'); ?>">PPDB</a></li>
			        <li class="drop-down <?php if($this->uri->segment(2) == "bkk") {echo 'active';} ?>"><a href="<?= base_url(''); ?>">BKK</a>
                  <ul>
                    <li class="<?php if($this->uri->segment(2) == "lowonganKerja") {echo 'active';} ?>"><a href="<?= base_url('Bkk/lowonganKerja'); ?>">Lowongan Kerja</a></li>
                    <li class="<?php if($this->uri->segment(2) == "serapanKerja") {echo 'active';} ?>"><a href="<?= base_url('Bkk/serapanKerja'); ?>">Serapan Kerja</a></li>
                  </ul>
			        </li>
              <li class="<?php if($this->uri->segment(2) == "contactUs") {echo 'active';} ?>"><a href="<?= base_url('Page/contactUs'); ?>">Hubungi Kami</a></li>
            </ul>
          </nav><!-- .nav-menu -->

          <a href="<?= base_url('Page/login') ?>" class="get-started-btn">Login</a>
        </div>
      </div>

    </div>
  </header><!-- End Header -->