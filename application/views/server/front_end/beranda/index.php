<?php foreach ($beranda as $d): ?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3>Kontrol Beranda</h3>
            </div>
            <div class="card-body">
                <form method="POST" action="<?= base_url('Admin_beranda/postEditBeranda/'.$d->id) ?>" id="form-edit" enctype="multipart/form-data">
                    <input type="hidden" name="lama_ikon" value="<?= $d->ikon ?>">
                    <input type="hidden" name="lama_banner" value="<?= $d->foto_banner ?>">
                    <input type="hidden" name="lama_kepala_sekolah" value="<?= $d->foto_kepala_sekolah ?>">
                <div class="mb-20">
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Nama Sekolah</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Nama Sekolah" value="<?= $d->nama_sekolah ?>" name="nama_sekolah">
                                <small class="text-danger"><i><?= form_error('nama_sekolah') ?></i></small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Ikon</label>
                        <div class="col-sm-8 col-lg-4">
                            <div class="input-group">
                                <input type="file" class="form-control" name="ikon">
                            </div>
                        </div>
                        <div class="col-sm-8 col-lg-6">
                            <img src="<?= base_url('assets/images/beranda_images/'.$d->ikon) ?>" alt="logo" width="40" height="40">
                            <small>*biarkan kosong jika tidak ingin diubah</small>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Teks Logo</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Teks Logo" value="<?= $d->teks_logo ?>" name="teks_logo">
                                <small class="text-danger"><i><?= form_error('teks_logo') ?></i></small>
                            </div>
                        </div>
                    </div>
                    <h4 class="title" align="center"><b>Banner Beranda</b></h4>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Judul Banner</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Judul Banner" name="judul_banner" value="<?= $d->judul_banner ?>">
                                <small class="text-danger"><i><?= form_error('judul_banner') ?></i></small>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Sub Judul Banner</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Sub Judul Banner" name="sub_judul_banner" value="<?= $d->sub_judul_banner ?>">
                                <small class="text-danger"><i><?= form_error('sub_judul_banner') ?></i></small>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <label class="col-sm-4 col-lg-2 col-form-label">Foto Banner</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <input type="file" class="form-control" name="foto_banner">
                            </div>
                            <small>*biarkan kosong jika tidak ingin diubah</small>
                            <div class="col-lg-12">
                                <img src="<?= base_url('assets/images/beranda_images/'.$d->foto_banner) ?>" class="img-fluid rounded">
                            </div>
                        </div>
                    </div>
                    <h4 class="title" align="center"><b>Sambutan Kepala Sekolah</b></h4>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Nama Kepala Sekolah</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Nama Kepala Sekolah" name="nama_kepala_sekolah" value="<?= $d->nama_kepala_sekolah ?>">
                                <small class="text-danger"><i><?= form_error('nama_kepala_sekolah') ?></i></small>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <label class="col-sm-4 col-lg-2 col-form-label">Foto Kepala Sekolah</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <input type="file" class="form-control" name="foto_kepala_sekolah">
                            </div>
                            <small>*biarkan kosong jika tidak ingin diubah</small>
                            <div class="col-lg-12">
                                <img src="<?= base_url('assets/images/beranda_images/'.$d->foto_kepala_sekolah) ?>" class="" width="250" height="250">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Deskripsi Sambutan</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <textarea class="form-control" name="deskripsi_sambutan" id="" cols="30" rows="10"><?= $d->deskripsi_sambutan ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Basic group add-ons end -->
                <!-- Icon Group Addons start -->
                <div>
                    <h4 class="title" align="center"><b>Visi & Misi</b></h4>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Deskripsi Visi</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <textarea class="form-control" name="deskripsi_visi" id="" cols="30" rows="5"><?= $d->deskripsi_visi ?></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 col-lg-2 col-form-label">Deskripsi Misi</label>
                        <div class="col-sm-8 col-lg-10">
                            <div class="input-group">
                                <textarea class="form-control" name="deskripsi_misi" id="" cols="30" rows="5"><?= $d->deskripsi_misi ?></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-right">                    
                    <button type="submit" class="btn btn-primary mr-2" id="btn-edit-submit">Submit</button>
                </div>
                <!-- Icon Group Addons end -->
                </form>
            </div>
        </div>
        <!-- Input group card end -->
        
    </div>
</div>
<?php endforeach ?>