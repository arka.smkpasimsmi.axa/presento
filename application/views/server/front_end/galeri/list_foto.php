<div class="page-header">
    <div class="row align-items-end">
        <div class="col-lg-8">
            <div class="page-header-title">
                <i class="ik ik-user" style="background-color: #6692e9;"></i>
                <div class="d-inline">
                    <h5>List Foto</h5>
                    <span>Kontrol Galeri Foto Sekolah disini</span>
                </div>
            </div>
        </div>
        <div class="col-lg-4">
            <nav class="breadcrumb-container" aria-label="breadcrumb">
                <button type="button" class="btn ml-auto text-light tema-biru" data-toggle="modal" data-target="#exampleModal">
                  <i class="ik ik-user-plus"></i>Tambah
                </button>
            </nav>
        </div>
    </div>
</div>
<div class="row">
<?php foreach($foto as $key) : ?>
<div class="col-md-4">
    <div class="card">
        <div class="card-body ">
            <div class="mb-20">
                <a href="<?= base_url('assets/images/galeri_images/'.$key->item); ?>" class="single-popup-photo"><img src="<?= base_url('assets/images/galeri_images/'.$key->item); ?>" class="rounded container crop-foto" width="250" height="250" ></a>
                <h5 class="mt-20 mb-0"><?= $key->judul; ?></h5>
                <p class="mt-2 text-secondary"><?= $key->deskripsi; ?></p>
            </div>
            <a href="<?= base_url('Galeri_sekolah/detailFoto/'.$key->id) ?>" class="badge badge-pill badge-info"><i class="ik ik-edit text-light"></i></a>
            <a href="<?= base_url('Galeri_sekolah/deleteGaleri/'.$key->id.'/foto') ?>" class="badge badge-pill badge-danger"><i class="ik ik-trash text-light"></i></a>
        </div>
    </div>
</div>
<?php endforeach; ?>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
            <form method="POST" action="<?= base_url('Galeri_sekolah/insertFoto') ?>" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="exampleInputUsername1">Judul Foto</label>
                    <input type="text" class="form-control" id="exampleInputUsername1" placeholder="Judul Foto" name="judul">
                    <small class="text-danger"><i><?= form_error('judul') ?></i></small>
                </div>
                <div class="form-group">
                    <label>Foto</label>
                    <div class="input-group col-xs-12">
                        <input type="file" class="form-control file-upload-info" placeholder="Upload Image" name="foto">
                        <span class="input-group-append">
                        <button class="file-upload-browse btn btn-primary" style="pointer-events: none;">Upload</button>
                        </span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="exampleTextarea1">Deskripsi Foto</label>
                    <textarea class="form-control" id="exampleTextarea1" rows="4" name="deskripsi"></textarea>
                </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn tema-biru text-light">Save changes</button>
        </form>
      </div>
</div>
</div>
</div>