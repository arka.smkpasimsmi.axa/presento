<!-- ======= Hero Section ======= -->
<?php foreach($beranda as $data) : ?>
<section id="hero" style="background: url('<?= base_url('assets/images/beranda_images/'.$data->foto_banner); ?>') top center;" class="d-flex align-items-center">
  <div class="container-fluid" data-aos="zoom-out" data-aos-delay="100">
    <div class="row justify-content-center">
      <div class="col-xl-10">
        <div class="row">
          <div class="col-xl-5">
            <h1><?= $data->judul_banner; ?></h1>
            <h2><?= $data->sub_judul_banner; ?></h2>
            <a href="#about" class="btn-get-started scrollto">Mulai</a>
          </div>
        </div>
      </div>
    </div>
  </div>

</section><!-- End Hero -->
<main id="main">
	
	<!-- ======= Clients Section ======= -->
	<section id="clients" class="clients">
		<div class="container-fluid" data-aos="zoom-in">
			<div class="row justify-content-center">
				<div class="col-xl-10">
				</div>
			</div>
		</div>
	</section><!-- End Clients Section -->
	
  <!-- ======= Sambutan Kepala Sekolah Section ======= -->
  <section id="about" class="about section-bg">
	  <div class="container" data-aos="fade-up">
		  
		  <div class="row no-gutters">
			  <div class="content col-xl-5 d-flex align-items-stretch">
				  <div class="content">
          <a href="<?= base_url('assets/images/beranda_images/'.$data->foto_kepala_sekolah); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->foto_kepala_sekolah; ?>">
				    <img style="width:100%;max-height: 500px;object-fit: cover;object-position: center;" src="<?= base_url('assets/images/beranda_images/'.$data->foto_kepala_sekolah); ?>" alt="<?= base_url('assets/images/beranda_images/profil_default.png'); ?>">
				  </a>	
            <h4 class="mt-5">Kepala Sekolah <?= $data->nama_sekolah; ?></h4>
				  	<h4><?= $data->nama_kepala_sekolah; ?></h4>
					</div>
				</div>
				<div class="col-xl-7 align-items-stretch mt-4">
					<div class="icon-boxes d-flex flex-column justify-content-center">
						<div class="row">
							<div class="col-md-12 icon-box" data-aos="fade-up" data-aos-delay="100">
								<h4>Sambutan Kepala Sekolah</h4>
								<p><?= $data->deskripsi_sambutan; ?></p>
							</div>
						</div>
					</div><!-- End .content-->
				</div>
			</div>
			
		</div>
	</section><!-- End Kepala Sekolah Section -->
	


  <!-- ======= Tabs Visi Misi ======= -->
  <section id="tabs" class="tabs">
    <div class="container" data-aos="fade-up">
	<div class="section-title">
        <h2>Visi & Misi</h2>
        <p>Visi dan Misi kami adalah</p>
      </div>
      <ul class="nav nav-tabs row d-flex">
        <li class="nav-item col-6">
          <a class="nav-link active show" data-toggle="tab" href="#tab-1">
            <i class="ri-gps-line"></i>
            <h4 class="d-none d-lg-block">Visi</h4>
          </a>
        </li>
        <li class="nav-item col-6">
          <a class="nav-link" data-toggle="tab" href="#tab-2">
            <i class="ri-body-scan-line"></i>
            <h4 class="d-none d-lg-block">Misi</h4>
          </a>
        </li>
      </ul>

      <div class="tab-content">
        <div class="tab-pane active show" id="tab-1">
          <div class="row">
            <div class="col-lg-6 order-2 order-lg-1 mt-3 mt-lg-0" data-aos="fade-up" data-aos-delay="100">
              <p class="font-italic">
                <?= $data->deskripsi_visi; ?>
              </p>
            </div>
            <div class="col-lg-6 order-1 order-lg-2 text-center" data-aos="fade-up" data-aos-delay="200">
            </div>
          </div>
        </div>
        <div class="tab-pane" id="tab-2">
          <div class="row">
			  <div class="col-lg-6 order-2 order-lg-2 mt-3 mt-lg-0">
				  <p class="font-italic">
					  <?= $data->deskripsi_misi; ?>
				  </p>
			  </div>
			  <div class="col-lg-6 order-1 order-lg-1 text-center">
			  </div>
          </div>
        </div>
        
      </div>
<?php endforeach; ?>
    </div>
  </section><!-- End Visi Misi Section -->

  <!-- ======= Galeri Section ======= -->
  <section id="portfolio" class="portfolio section-bg">
    <div class="container" data-aos="fade-up">

      <div class="section-title">
        <h2>Galeri Kegiatan Sekolah</h2>
        <p>Berikut merupakan kegiatan dari <?= $data->nama_sekolah; ?></p>
	  </div>
	  
      <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
	<?php foreach($galeri_kegiatan as $data) : ?>

        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="<?= base_url('assets/images/galeri_images/'.$data->item); ?>" class="img-fluid" alt="<?= base_url('assets/images/galeri_images/'.$data->item); ?>">
            <div class="portfolio-info">
              <h4><?= $data->judul; ?></h4>
              <p style="width:100%;height:45px;overflow:hidden;text-overflow:ellipsis;"><?= $data->deskripsi; ?></p>
              <div class="portfolio-links">
                <a href="<?= base_url('assets/images/galeri_images/'.$data->item); ?>" data-gall="portfolioGallery" class="venobox"
                  title="<?= $data->item; ?>"><i class="bx bx-search"></i></a>
                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
              </div>
            </div>
          </div>
        </div>
		<?php endforeach; ?>
	</div>
	<div class="col-12 justify-content-lg-center text-center mt-4">
		<a href="<?= base_url('Page/foto') ?>">Lihat Semua ></a>
	</div>

    </div>
  </section>
  <!-- End Galeri Section -->

  <!-- ======= Prestasi Section ======= -->
  <section id="portfolio" class="portfolio">
    <div class="container" data-aos="fade-up">
	<?php foreach($beranda as $data) : ?>
      <div class="section-title">
        <h2>Prestasi</h2>
        <p>Berikut merupakan prestasi dari <?= $data->nama_sekolah; ?>.</p>
      </div>
	<?php endforeach; ?>
	
      <div class="row portfolio-container" data-aos="fade-up" data-aos-delay="200">
	<?php foreach($prestasi as $data) : ?>
        <div class="col-lg-4 col-md-6 portfolio-item filter-app">
          <div class="portfolio-wrap">
            <img src="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" class="img-fluid" alt="">
            <div class="portfolio-info">
              <h4><?= $data->nama_prestasi; ?></h4>
              <p style="width:100%;height:45px;overflow:hidden;text-overflow:ellipsis;"><?= $data->deskripsi; ?></p>
              <div class="portfolio-links">
                <a href="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" data-gall="portfolioGallery" class="venobox"
                  title="<?= $data->foto ?>"><i class="bx bx-search"></i></a>
                <a href="portfolio-details.html" title="More Details"><i class="bx bx-link"></i></a>
              </div>
            </div>
          </div>
		</div>
	<?php endforeach; ?>	
	  </div>
	  <div class="col-12 justify-content-lg-center text-center mt-4">
		<a href="<?= base_url('Page/prestasi') ?>">Lihat Semua ></a>
	</div>

    </div>
  </section><!-- End Prestasi Section -->


  <!-- ======= Guru Section ======= -->
  <section id="team" class="team section-bg">
    <div class="container" data-aos="fade-up">
		<?php foreach($beranda as $data) : ?>
		<div class="section-title">
			<h2>Profil Guru</h2>
			<p>Berikut merupakan profil guru dari <?= $data->nama_sekolah; ?>.</p>
		</div>
		<?php endforeach; ?>
		
		<div class="row">
	<?php foreach ($guru as $data) : ?>	
	
        <div class="col-lg-3 col-md-6 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
              <img src="<?= base_url('assets/images/guru_images/'.$data->foto); ?>" class="img-fluid" alt="">
              <div class="social">
                <a href=""><i class="icofont-twitter"></i></a>
                <a href=""><i class="icofont-facebook"></i></a>
                <a href=""><i class="icofont-instagram"></i></a>
                <a href=""><i class="icofont-linkedin"></i></a>
              </div>
            </div>
            <div class="member-info">
              <h4><?= $data->nama_guru; ?></h4>
              <span><?= $data->mapel; ?></span>
            </div>
          </div>
		</div>
    <div class="col-12 justify-content-lg-center text-center mt-4">
		  <a href="<?= base_url('Page/guru') ?>">Lihat Semua ></a>
	  </div>
		
	<?php endforeach; ?>

      </div>

    </div>
  </section><!-- End Guru Section -->

  <!-- ======= Contact Section ======= -->
  <section id="contact" class="contact">
    <div class="container" data-aos="fade-up">

    </div>
  </section><!-- End Contact Section -->

</main><!-- End #main -->