 
 <!-- ======= prestasi Section ======= -->
 <section id="team" class="team">
    <div class="container mt-5" data-aos="fade-up">
		<?php foreach($beranda as $data) : ?>
		<div class="section-title">
			<h2>Prestasi Sekolah</h2>
			<p>Berikut merupakan kumpulan prestasi dari <?= $data->nama_sekolah; ?>.</p>
		</div>
		<?php endforeach; ?>
		
		<div class="row">
	<?php foreach ($prestasi as $data) : ?>	
	
        <div class="col-lg-4 col-md-4 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
				<a href="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->foto; ?>">
              		<img style="width:100%;max-height: 230px;object-fit: cover;object-position: center;cursor:pointer;" src="<?= base_url('assets/images/prestasi_images/'.$data->foto); ?>" class="img-fluid" alt="">
				</a>
			</div>
            <div class="member-info">
              <p>Tanggal :<?= date('d-m-Y', strtotime($data->created_dt)); ?></p>
              <h4><?= $data->nama_prestasi; ?></h4>
              <p><?= $data->deskripsi; ?></p>
            </div>
          </div>
		</div>
		
	<?php endforeach; ?>

      </div>

    </div>
  </section><!-- End prestasi Section -->