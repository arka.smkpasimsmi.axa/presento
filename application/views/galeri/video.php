 
 <!-- ======= video Section ======= -->
 <section id="team" class="team">
    <div class="container mt-5" data-aos="fade-up">
		<?php foreach($beranda as $data) : ?>
		<div class="section-title">
			<h2>Galeri Video Kegiatan Sekolah</h2>
			<p>Berikut merupakan galeri video kegiatan dari <?= $data->nama_sekolah; ?>.</p>
		</div>
		<?php endforeach; ?>
		
		<div class="row">

		<?php  foreach ($video as $data) : ?>
		<?php $url = $data->item;  ?>
		<?php $value = explode("v=", $url); ?>
		<?php $videoId = $value[1]; ?>
	
        <div class="col-lg-4 col-md-4 d-flex align-items-stretch">
          <div class="member" data-aos="fade-up" data-aos-delay="100">
            <div class="member-img">
				<iframe width="100%" height="200px" src="https://www.youtube.com/embed/<?= $videoId; ?>"></iframe>
			</div>
            <div class="member-info">
			  <h4><?= $data->judul ?></h4>
              <p><?= $data->deskripsi; ?></p>
            </div>
          </div>
		</div>
		
	<?php endforeach; ?>

      </div>

    </div>
  </section><!-- End video Section -->