<!-- ======= Breadcrumbs ======= -->
<section class="breadcrumbs">
      <div class="container">

        <ol>
          <li><a href="<?= base_url('Bkk/serapanKerja') ?>">Serapan Kerja</a></li>
          <li>Detail Serapan Kerja</li>
        </ol>
        <h2>Detail Serapan Kerja</h2>

      </div>
    </section><!-- End Breadcrumbs -->

    <!-- ======= Portfolio Details Section ======= -->
    <section id="portfolio-details" class="portfolio-details">
      <div class="container" data-aos="fade-up">
        <div class="portfolio-details-container">
        <?php foreach($serapan as $data) : ?>
          <div class="owl-carousel portfolio-details-carousel">
          <a href="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" data-gall="portfolioGallery" class="venobox" title="<?= $data->item; ?>">
            <img src="<?= base_url('assets/images/bkk_images/'.$data->item); ?>" class="img-fluid" alt="">
          </a>
        </div>
        <?php endforeach; ?>
          <div class="portfolio-info">
            <h3>Informasi Perusahaan</h3>
            <ul>
              <li><strong>Kota</strong>: <?= $data->alamat ?></li>
              <li><strong>Alamat</strong>: <?= $data->kota ?></li>
              <li><strong>No.Telp</strong>: <?= $data->no_telp ?></li>
              <li><strong>Email</strong>: <a href=""><?= $data->email_perusahaan ?></a></li>
            </ul>
          </div>

        </div>

        <?php foreach($serapan as $data) : ?>
        <div class="portfolio-description">
          <h2><?= $data->judul ?></h2>
          <p>
          <?= $data->deskripsi ?>
          </p>
        </div>
        <?php endforeach; ?>
      </div>
    </section><!-- End Portfolio Details Section -->